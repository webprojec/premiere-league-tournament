<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clubs;
use App\Models\Matches;
use App\Services\GenerateMatchCombinations;

class AjaxController extends Controller
{
    public function populate() {
        $response = $_POST['clubNames'];
        foreach ($response as $club) {
            $query = Clubs::insert([
                "name" => $club,
                "total_points" => 0,
                "played" => 0,
                "won" => 0,
                "drown" => 0,
                "lost" => 0,
                "GD" => 0,
            ]);
            if (!$query) {
                return response()->json(false);
            }
        }
        return response()->json(true);
    }

    public function arrangeMatches() 
    {   
        $club_names = $_POST['clubNames'];
        $club_count = sizeof($club_names);
        if(Matches::all()->isEmpty()) {
            $raw_combinations = makeMatchCombinations ($club_names);
            $arranged_array = new GenerateMatchCombinations($raw_combinations, $club_count);
            Matches::insert($arranged_array->get());
            return response()->json(true);
        }
        
    }
}
