<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clubs;
use App\Models\Matches;
use App\Services\GenerateIndexViewVars;
use App\Services\GenerateMatchesResultViewVars;

class IndexController extends Controller
{
    protected $club_num;
    public function __construct()
    {
        $club_count = 0;
        foreach (Clubs::all() as $club) {
            $club_count += 1;
        }
        $this->club_num = $club_count;
    }

    public function index(Request $request)
    {
        $last_week_last_club = Matches::orderBy("id", "desc")
            ->first()
            ->toArray();
        $max_weeks = $last_week_last_club["week_num"];

        if ($request->week_num > $max_weeks) {
            $playAll = $request->playAll
                ? "play-all-matches/" . $request->week_num
                : null;
            
            if (!$playAll) {
                return redirect("week/end");
            } else {
                return redirect("play-all-matches/" . $request->week_num);
            }
        }
        $indexViewVars = new GenerateIndexViewVars($request, $this->club_num);
        return view("index", $indexViewVars->get());
    }

    public function playAllMatches(Request $request)
    {
        $last_week_last_club = Matches::orderBy("id", "desc")
            ->first()
            ->toArray();
        $max_weeks = $last_week_last_club["week_num"];

        if ($request->week_num <= $max_weeks) {
            $request->playAll = true;
            $request->week_num += 1;
            return $this->index($request);
        } else {
            return redirect("play-all-matches/results/" . $max_weeks);
        }
    }

    public function showAllResults(Request $request)
    {
        $matchesResultViewVars = new GenerateMatchesResultViewVars($request, $this->club_num);
        return view("matches-result", $matchesResultViewVars->get());
    }
}
