<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clubs extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $table = 'pl_clubs';

    protected $fillable = [
        'name',
        'total_points',
        'played',
        'won',
        'drown',
        'lost',
        'GD'
    ];
}
