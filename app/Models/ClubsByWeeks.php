<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubsByWeeks extends Model
{
    use HasFactory;

    protected $table = 'pl_clubs_by_weeks';

    public $timestamps = false;
    
    protected $fillable = [
        'week_num',
        'name',
        'total_points',
        'played',
        'won',
        'drown',
        'lost',
        'GD'
    ];
}
