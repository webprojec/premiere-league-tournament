<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
    use HasFactory;

    protected $table = 'pl_matches';

    public $timestamps = false;

    protected $fillable = [
        'team1',
        'team1_result',
        'team2',
        'team2_result'
    ];
}
