<?php

namespace App\Services;

use App\Models\Clubs;
use App\Models\Matches;

class CalculateWinPercentages
{
    protected $win_percentages;

    public function __construct()
    {
        $total_week_num = Matches::orderBy("id", "desc")->first()->week_num;
        $current_week_records = Clubs::all();
        $win_percentages = [];

        foreach ($current_week_records as $club) {
            array_push(
                $win_percentages,
                floor(($club->total_points * 100) / ($total_week_num * 3))
            );
        }
        $this->win_percentages = $win_percentages;
    }

    public function get()
    {
        return $this->win_percentages;
    }
}