<?php

namespace App\Services;

use App\Models\Clubs;

class GenerateMatchCombinations
{
    protected $arranged_array;
    
    public function __construct($raw_combinations, $club_count)
    {
        $sent_array = [];
        $counter = 0;
        $week_counter = 1;
        $arranged_array = [];
        while ($raw_combinations) {
            for ($i = 0; $i < sizeof($raw_combinations); $i++) {
                $team2 = reset($raw_combinations[$i]);
                $team1 = key($raw_combinations[$i]);

                if (count($sent_array) !== 0) {
                    $recently_sent = null;
                    $teams_played = false;
                    foreach ($sent_array as $match) {
                        $value = reset($match);
                        $key = key($match);
                        if (
                            $team1 === $key ||
                            $team1 === $value ||
                            $team2 === $key ||
                            $team2 === $value
                        ) {
                            $teams_played = true;
                        }
                    }
                    if (!$teams_played) {
                        array_push($sent_array, [
                            $team1 => $team2,
                        ]);
                        array_push($arranged_array, [
                            "week_num" => $week_counter,
                            "team1" => $team1,
                            "team2" => $team2,
                        ]);

                        // remove picked up matches from raw combinations and reindex it's values
                        unset($raw_combinations[$i]);
                        $raw_combinations = array_values($raw_combinations);
                        $counter += 1;
                    }
                    if ($counter === $club_count / 2) {
                        // if $club_count/2 matches already, 1 week => $club_count/2 matches
                        $counter = 0;
                        $sent_array = [];
                        $week_counter += 1;
                    }
                } elseif (count($sent_array) === 0) {
                    array_push($sent_array, [
                        $team1 => $team2,
                    ]);
                    array_push($arranged_array, [
                        "week_num" => $week_counter,
                        "team1" => $team1,
                        "team2" => $team2,
                    ]);

                    // remove picked up matches from raw combinations and reindex it's values
                    unset($raw_combinations[$i]);
                    $raw_combinations = array_values($raw_combinations);
                    $counter += 1;
                }
            }
        }
        $this->arranged_array = $arranged_array;
    }

    public function get()
    {
        return $this->arranged_array;
    }
}