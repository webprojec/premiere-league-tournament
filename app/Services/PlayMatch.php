<?php

namespace App\Services;

use App\Models\Clubs;
use App\Models\Matches;

class PlayMatch
{
    protected $response_array;

    public function __construct($t1, $t2)
    {
        $t1_char = Clubs::select(
            "total_points",
            "played",
            "won",
            "drown",
            "lost",
            "GD"
        )
            ->where("name", "=", $t1)
            ->first();
        $t2_char = Clubs::select(
            "total_points",
            "played",
            "won",
            "drown",
            "lost",
            "GD"
        )
            ->where("name", "=", $t2)
            ->first();

        // (total_points + won + draw + home + 1) / (played + lost + gd + away + 1)
        // +1 in numerator & denominators is to avoid 0 in the 1st week
        // team1 will always have home = 1 & away = 0, team2 is vice versa
        $t1_result =
            ($t1_char->total_points + $t1_char->won + $t1_char->draw + 2) /
            ($t1_char->played + $t1_char->lost + $t1_char->GD + 1); // home + 1 = 2 in numerator
        $t2_result =
            ($t2_char->total_points + $t2_char->won + $t2_char->draw + 1) /
            ($t2_char->played + $t2_char->lost + $t2_char->GD + 2); // away + 1 = 2 in denominator

        // seed with microseconds
        srand(makeSeed());
        $t1_result = round($t1_result + (rand() % 2));
        srand(makeSeed());
        // t1/t2 = 2/0.5 in week #1
        // so made results more or less random with %4 to leave chance for second team
        $t2_result = round($t2_result + (rand() % 4));

        $t1_tp = $t1_char->total_points;
        $t1_won = $t1_char->won;
        $t1_drown = $t1_char->drown;
        $t1_lost = $t1_char->lost;
        $t2_tp = $t2_char->total_points;
        $t2_won = $t2_char->won;
        $t2_drown = $t2_char->drown;
        $t2_lost = $t2_char->lost;

        if ($t1_result > $t2_result) {
            $t1_tp += 3;
            $t1_won += 1;
            $t2_lost += 1;
        } elseif ($t1_result < $t2_result) {
            $t2_tp += 3;
            $t2_won += 1;
            $t1_lost += 1;
        } else {
            // draw
            $t1_tp += 1;
            $t1_drown += 1;
            $t2_tp += 1;
            $t2_drown += 1;
        }

        $response_array = [
            "team1" => [
                "name" => $t1,
                "result" => $t1_result,
                "total_points" => $t1_tp,
                "played" => $t1_char->played + 1,
                "won" => $t1_won,
                "drown" => $t1_drown,
                "lost" => $t1_lost,
                "GD" => $t1_char->GD + $t1_result,
            ],
            "team2" => [
                "name" => $t2,
                "result" => $t2_result,
                "total_points" => $t2_tp,
                "played" => $t2_char->played + 1,
                "won" => $t2_won,
                "drown" => $t2_drown,
                "lost" => $t2_lost,
                "GD" => $t2_char->GD + $t2_result,
            ],
        ];
        $this->response_array = $response_array;
    }

    public function get()
    {
        return $this->response_array;
    }
}