<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlClubsByWeeks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("pl_clubs_by_weeks", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("week_num");
            $table->string("name");
            $table->integer("total_points");
            $table->integer("played");
            $table->integer("won");
            $table->integer("drown");
            $table->integer("lost");
            $table->integer("GD");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("pl_clubs_by_weeks");
    }
}
