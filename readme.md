
# Football Tournament Imitation

  

Application that will imitate football tournament with several matches per week, calculate outcome based on earned points, total goal number, and many more factors. Each weeks it will show prediction of winning for each team in percents based on the information it has so far. User can use 'Play All' to see final results

  

## To run

  

* Rename `.env.example` file to just `.env`

* Go ahead and update `DB_DATABASE` attribute to `premiere_league`

* `DB_` attributes should look like this:

>`DB_CONNECTION=mysql`<br/>
>`DB_HOST=127.0.0.1`<br/>
>`DB_PORT=3306`<br/>
>`DB_DATABASE=premiere_league`<br/>
>`DB_USERNAME=root`<br/>
>`DB_PASSWORD=`

* Open terminal and type the following comands:

	- `php artisan migrate:reset`

	- `php artisan migrate:refresh`

	- `php artisan serve`

* In your browser, go to [localhost:8000/](localhost:8000/) or [127.0.0.1:8000](127.0.0.1:8000/)

* Enter clubs' names and click "Finish"
