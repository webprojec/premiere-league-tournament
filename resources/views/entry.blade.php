<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Setup Clubs</title>
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/setup.css">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<body>
    <div class="main">
        <div class="entry-header">
            <h2><em>Enter Club Names</em></h2>
        </div>
        <div class="center">
            <div class="setup-container">
                <div class="input-container">
                    <div class="single-input">
                        <input class="textfield" type="text">
                        <button hidden>Add More</button>
                    </div>
                    <div class="single-input">
                        <input class="textfield" type="text">
                        <button hidden>Add More</button>
                    </div>
                    <div class="single-input">
                        <input class="textfield" type="text">
                        <button hidden>Add More</button>
                    </div>
                    <div class="single-input">
                        <input class="textfield" type="text">
                        <button hidden class="add-more-button">Add More</button>
                    </div>
                </div>
                <button class="finish-button" onclick="sendValuesToDB()">Finish</button>
            </div>
        </div>
    </div>
    <script>
        // redefineEventListeners();
        // function redefineEventListeners() {
        //     var inputElements = document.getElementsByClassName("add-more-button");
        //     Array.prototype.forEach.call(inputElements, element => {
        //         element.addEventListener("click", addMoreTextfields);
        //     });
        // }
        
        // function addMoreTextfields(event) {
        //     event.target.style.visibility = "hidden";
        //     document.querySelector(".input-container").insertAdjacentHTML('beforeend', 
        //         "<div class='single-input'> <input class='textfield' type='text'> </div>"
        //     );
        //     document.querySelector(".input-container").insertAdjacentHTML('beforeend', 
        //         "<div class='single-input'> <input class='textfield' type='text'> <button class='add-more-button'> Add More </button> </div>"
        //     );
        //     redefineEventListeners();
        // }

        function sendValuesToDB() {
            var clubNames = [];
            var textfields = document.getElementsByClassName("textfield");
            Array.prototype.forEach.call(textfields, element => {
                if(element.value !== "" && clubNames.indexOf(element.value) === -1) {
                    clubNames.push(element.value);
                }
            });
            $.ajax({
               type:'POST',
               url:'/populate-db',
               data: {
                   clubNames: clubNames,
                   _token: "{{ csrf_token() }}" 
               },
               success: function(insertedClubs) {
                   if (insertedClubs) {
                        $.ajax({
                            type:'POST',
                            url:'arrange-matches',
                            data: {
                                clubNames: clubNames,
                                _token: "{{ csrf_token() }}" 
                            },
                            success: function(matchesArranged) {
                                if (matchesArranged) {
                                    window.location.href = "week/1";
                                }
                            }
                        });
                   }
               }
            });
        }
    </script>
</body>
</html>