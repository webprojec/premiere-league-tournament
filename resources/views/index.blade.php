<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Premiere League</title>
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	@php
	if ($msg) {
		echo "<script>alert('$msg');</script>";
	}
	@endphp
</head>
<body>
	@if ($msg)
	<form action="/week/1"><button hidden>Next Week</button></form>
	<script>document.querySelector("button").click();</script>
	@endif
	<div class="main">
		<div class="week-header">
			<h2><em>Week {{$current_week}}</em></h2>
		</div>
		<div class="center">
			<div class="statistics-container">
				<table class="statistics">
					<thead>
						<tr>
							<th class="global-header" colspan="7">League Table</th>
							<th class="global-header" colspan="3">Match Results</th>
						</tr>
					</thead>
					<thead>
						
						<tr>
							<th class="cell teams">Teams</th>
							<th class="cell">PTS</th>
							<th class="cell">P</th>
							<th class="cell">W</th>
							<th class="cell">D</th>
							<th class="cell">L</th>
							<th class="cell">GD</th>
							@once
							<th colspan="3">{{$current_week}}<sup>{{$ordinal_suffix}}</sup> Week Match Results</th>
							@endonce
						</tr>
					</thead>
					<tbody>
						@foreach ($clubs as $key => $club)
						<tr>
							<td class="cell teams">{{$club -> name}}</td>
							<td class="cell">{{$club -> total_points}}</td>
							<td class="cell">{{$club -> played}}</td>
							<td class="cell">{{$club -> won}}</td>
							<td class="cell">{{$club -> drown}}</td>
							<td class="cell">{{$club -> lost}}</td>
							<td class="cell">{{$club -> GD}}</td>
							@if ($key < $matches_per_week)
							<td class = "no-borders to-left">{{$matches[$key]["team1"]}}</td>
							<td class = "no-borders result">{{$matches[$key]["team1_result"] . " - " . $matches[$key]["team2_result"]}}</td>
							<td class = "no-borders to-right">{{$matches[$key]["team2"]}}</td>
							@else
							<td class = "no-borders"></td>
							<td class = "no-borders"></td>
							<td class = "no-borders"></td>
							@endif
						</tr>
						@endforeach
					</tbody>					
					<tfoot>
						<tr>
							<th>
								<form action={{"/play-all-matches/" . $current_week}}>
									<button>Play All</button>
								</form>
							</th>
							<th colspan="8"></th>
							<th>
								<form action={{"/week/" . ($current_week+1)}}>
									<button>Next Week</button>
								</form>
								
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="predictions-container">
				<table class="predictions">
					<tr>
						@once
						<th class="secondary-header" colspan="2">{{$current_week}}<sup>{{$ordinal_suffix}}</sup> Week Predictions of Championship</th>
						@endonce
					</tr>
					@foreach($clubs as $key => $club)
					<tr>
						<td class="teams">{{$club -> name}}</td>
						<td>%{{$percentages[$key]}}</td>
					</tr>
					@endforeach
				</table>
			</div>
			
		</div>
	</div>
	@if ($playAll)
	<script>document.querySelector("button").click();</script>
	@endif
</body>
</html>